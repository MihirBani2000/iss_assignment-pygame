# ISS Assignment 3 
### Game Development using Python and PyGame 
## _**BOND Escape**_
> _Mihir Bani  
> 2019113003_

### Hello there! This is a small game developed by me for my course assignment. Read the instructions here and try out the game. I hope you like it :)  


### Gameplay and Rules :  

The game has two rounds, each round consists of both the players playing once against each other. 

Player 1 is at bottom and has to reach the other end by avoiding all the obstacles and in minimum time.  

Player 2 starts at the top of the screen and has to reach the bottom, the other objectives remain the same.  
As the player crosses a band, he/she is rewarded points according to the number of obstacles in that band, the rules are –   
1. +5 for fixed obstacles  
1. +10 for moving obstacles  

The player with the greater score wins that particular round, but if the scores are equal than the player taking less time to attain that score wins. 

In the next round, the speed of the moving obstacles increases for the winner of the previous round.  
 
The game automatically closes after the second round. (Just to stop you from being very competitive and addicted to it !!).   

### Controls: 

Keyboard `Arrow Keys` to move up, down, left and right.  
Pressing `SpaceBar` along with the movement keys gives a speed boost.  


### How to install and run on Linux: 

1. Download the folder/copy it to your local device. 
1. Install python, pygame 
	1. Check this link to get them installed on linux – https://linuxize.com/post/how-to-install-python-3-7-on-ubuntu-18-04/ 
	1. Download PyGame – https://www.pygame.org/download.shtml 
	1. Further help - https://www.pygame.org/wiki/GettingStarted
1. Open the folder in Terminal and run : 
	1. 	`python3 main.py` 
1. Enjoy the Game.  

### How to install and run on Windows: 

1. Download the folder/copy it to your local device. 
1. Install python, pygame 
	1. Download python3  – https://www.python.org/downloads/release/python-376/
	1. Download PyGame – https://www.pygame.org/download.shtml 
	1. Further help - https://www.pygame.org/wiki/GettingStarted
1. Open the folder in Python IDE or some other IDE like PyCharm or code editor like VS CODE and run : 
	>https://www.jetbrains.com/pycharm/download/  
	>https://code.visualstudio.com/download
	1. `python3 main.py` 
1. Enjoy the Game. 


#### Attributes
In this game I have used `Python 3` and `PyGame`.

These are some resources I used for icons.
Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a
        href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>  
Icons made by <a href="https://www.flaticon.com/authors/surang" title="surang">surang</a> from <a
        href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>  
Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a
        href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>  

For the game development i have used PyCharm by JetBrains https://www.jetbrains.com/pycharm/download/
#### _BOND Escape_
The name BOND is very familier. This game not only takes inspiration from him (you heard me), but also takes inspiration from a common jargon used in the IIIT Hyd student community. Here, a _BOND_ means a person who is pro at skill.
