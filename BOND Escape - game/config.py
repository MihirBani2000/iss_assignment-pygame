# importing the libraries
import pygame as pg
import math
from pygame import mixer

# Initializing the pygame
pg.init()

# create the screen with resolution 1280*720
screen = pg.display.set_mode((1280, 720))

# initializing clock
clock = pg.time.Clock()

# Caption and Icon
pg.display.set_caption("BOND Escape")
icon = pg.image.load('icon.png')
pg.display.set_icon(icon)

# sounds
mixer.music.load("SEASHORE.mp3")
mixer.music.play(-1)
collision_sound = mixer.Sound("collide.wav")
win_sound = mixer.Sound("cheer.wav")

# fonts
start_font = pg.font.Font('freesansbold.ttf', 60)
end_font = pg.font.Font('freesansbold.ttf', 60)
pchange_font = pg.font.Font('freesansbold.ttf', 40)
round_font = pg.font.Font('freesansbold.ttf', 40)
score_font = pg.font.Font('freesansbold.ttf', 22)
controls_font = pg.font.Font('freesansbold.ttf', 38)

# kn =  speed multiplier for nth player
k1 = 1
k2 = 1

# time taken by the players
round1_time = [0, 0]
round2_time = [0, 0]

# score
score_round1 = []
score_round2 = []
score_round1.append(0)
score_round1.append(0)
score_round2.append(0)
score_round2.append(0)

# score flag - check condition for updating the score if the player crosses a band
flag1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
flag2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
p1_check = [585, 520, 455, 390, 325, 260, 195, 130, 65, 0]
p1_score_arr = [2 * 5, 2 * 10, 4 * 5, 2 * 10, 2 * 5 + 10, 2 * 10, 2 * 5 + 10, 2 * 10, 2 * 5 + 10, 2 * 10]
p2_check = [65, 130, 195, 260, 325, 390, 455, 520, 585, 650]
p2_score_arr = [2 * 5, 2 * 10, 2 * 5 + 10, 2 * 10, 2 * 5 + 10, 2 * 10, 2 * 5 + 10, 2 * 10, 4 * 5, 2 * 10]

# Player List initialize
pImg = []
pX = []
pY = []
pX_delta = []
pY_delta = []

# init Player1 = p[0]
pImg.append(pg.image.load('player1.png'))
pX.append(540)
pY.append(656)
pX_delta.append(0)
pY_delta.append(0)

# init Player2 = p[1]
pImg.append(pg.image.load('player2.png'))
pX.append(660)
pY.append(0)
pX_delta.append(0)
pY_delta.append(0)

# init fixed obstacles
fixed_obsImg = []
fixed_obsX = []
fixed_obsY = []
tree = []
num_fixed = 15
tree.append(pg.image.load('tree.png'))
tree.append(pg.image.load('tree1.png'))
tree.append(pg.image.load('bush.png'))
tree.append(pg.image.load('tree2.png'))

# initialize moving obstacles on ground
ground_obsImg = []
ground_obsX = []
ground_obsY = []
ground_obsX_delta = []
num_ground = 3

# initialize moving obstacles on water
water_obsImg = []
water_obsX = []
water_obsY = []
water_obsX_delta = []
num_water = 11
boat = [pg.image.load('submarine.png'), pg.image.load('ship.png'), pg.image.load('boat.png')]

# fixed obstacles - setting icons and positions
for i in range(num_fixed):
    fixed_obsImg.append(tree[i % 4])

fixed_obsX.append(350)
fixed_obsY.append(0)
fixed_obsX.append(850)
fixed_obsY.append(0)
fixed_obsX.append(120)
fixed_obsY.append(130)
fixed_obsX.append(450)
fixed_obsY.append(130)
fixed_obsX.append(100)
fixed_obsY.append(260)
fixed_obsX.append(750)
fixed_obsY.append(260)
fixed_obsX.append(1100)
fixed_obsY.append(260)
fixed_obsX.append(300)
fixed_obsY.append(390)
fixed_obsX.append(1050)
fixed_obsY.append(390)
fixed_obsX.append(0)
fixed_obsY.append(520)
fixed_obsX.append(400)
fixed_obsY.append(520)
fixed_obsX.append(800)
fixed_obsY.append(520)
fixed_obsX.append(1150)
fixed_obsY.append(520)
fixed_obsX.append(250)
fixed_obsY.append(655)
fixed_obsX.append(850)
fixed_obsY.append(655)

# moving obstacles ground - setting icons and positions
# 0
ground_obsImg.append(pg.image.load('scorpio.png'))
ground_obsX.append(514)
ground_obsY.append(130)
ground_obsX_delta.append(4)
# 1
ground_obsImg.append(pg.image.load('hyena.png'))
ground_obsX.append(164)
ground_obsY.append(260)
ground_obsX_delta.append(4)
# 2
ground_obsImg.append(pg.image.load('scorpio.png'))
ground_obsX.append(364)
ground_obsY.append(390)
ground_obsX_delta.append(4)

# moving obstacles water - setting icons and positions
for i in range(num_water):
    water_obsImg.append(boat[i % 3])
    water_obsX_delta.append(4.5)

water_obsX.append(50)
water_obsY.append(65)
water_obsX.append(700)
water_obsY.append(65)
water_obsX.append(150)
water_obsY.append(195)
water_obsX.append(550)
water_obsY.append(195)
water_obsX.append(950)
water_obsY.append(195)
water_obsX.append(800)
water_obsY.append(325)
water_obsX.append(300)
water_obsY.append(325)
water_obsX.append(0)
water_obsY.append(455)
water_obsX.append(500)
water_obsY.append(455)
water_obsX.append(650)
water_obsY.append(585)
water_obsX.append(100)
water_obsY.append(585)

# arrow and spacebar icons
arrow_icon = pg.image.load('arrow.png')
spacebar_icon = pg.image.load('spacebar.png')


# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------
# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------

# player position reset after rounds
def p_reset():
    pX[0] = 540
    pX[1] = 660
    pY[0] = 656
    pY[1] = 0
    pX_delta[0] = 0
    pY_delta[0] = 0
    pX_delta[1] = 0
    pY_delta[1] = 0
    # flag for scoring
    for j in range(10):
        flag1[j] = 0
        flag2[j] = 0


# show controls text
def show_controls():
    arrow_text = controls_font.render("Movement", True, (100, 50, 100))
    spacebar_text = controls_font.render("Speed Boost", True, (100, 50, 100))
    screen.blit(arrow_icon, (425, 450))
    screen.blit(arrow_text, (575, 500))
    screen.blit(spacebar_icon, (415, 570))
    screen.blit(spacebar_text, (565, 610))


# show player score
def show_score(num_round):
    p1_text = score_font.render("Player 1", True, (255, 255, 255))
    p2_text = score_font.render("Player 2", True, (255, 255, 255))
    round1_text = score_font.render("Round 1", True, (255, 255, 255))
    round2_text = score_font.render("Round 2", True, (255, 255, 255))

    if num_round <= 1:
        p1_score_text = score_font.render("Score: " + str(score_round1[0]), True, (255, 255, 255))
        p2_score_text = score_font.render("Score: " + str(score_round1[1]), True, (255, 255, 255))
        screen.blit(round1_text, (5, 5))
        timer1_text = score_font.render("Timer: " + str(round1_time[0]), True, (255, 255, 255))
        timer2_text = score_font.render("Timer: " + str(round1_time[1]), True, (255, 255, 255))
        screen.blit(timer1_text, (5, 695))
        screen.blit(timer2_text, (1170, 60))
    else:
        p1_score_text = score_font.render("Score: " + str(score_round2[0]), True, (255, 255, 255))
        p2_score_text = score_font.render("Score: " + str(score_round2[1]), True, (255, 255, 255))
        screen.blit(round2_text, (5, 5))
        timer1_text = score_font.render("Timer: " + str(round2_time[0]), True, (255, 255, 255))
        timer2_text = score_font.render("Timer: " + str(round2_time[1]), True, (255, 255, 255))
        screen.blit(timer1_text, (5, 695))
        screen.blit(timer2_text, (1170, 60))
    screen.blit(p1_text, (5, 640))
    screen.blit(p1_score_text, (5, 667))
    screen.blit(p2_text, (1170, 5))
    screen.blit(p2_score_text, (1170, 32))


# game start screen
def start_screen():
    screen.fill((255, 0, 255))
    start_text = start_font.render("Hi! Press ENTER to Play!!", True, (0, 0, 0))
    pchange1_text = pchange_font.render("Player 1, Get READY!!", True, (100, 50, 100))
    screen.blit(start_text, (275, 300))
    screen.blit(pchange1_text, (425, 380))
    show_controls()
    pg.display.update()


# game main screen
def game_background():
    screen.fill((50, 150, 255))
    pg.draw.rect(screen, (56, 105, 69), (0, 0, 1280, 65))
    pg.draw.rect(screen, (56, 105, 69), (0, 130, 1280, 65))
    pg.draw.rect(screen, (56, 105, 69), (0, 260, 1280, 65))
    pg.draw.rect(screen, (56, 105, 69), (0, 390, 1280, 65))
    pg.draw.rect(screen, (56, 105, 69), (0, 520, 1280, 65))
    pg.draw.rect(screen, (56, 105, 69), (0, 650, 1280, 70))


# player change screen
def pchange_screen(num_round):
    screen.fill((155, 195, 50))
    pchange_text = pchange_font.render("Player 2, Get READY!!", True, (255, 255, 255))
    screen.blit(pchange_text, (275, 320))
    show_score(num_round)
    pg.display.update()


# round change screen
def round_screen(win):
    mixer.music.pause()
    win_sound.play()
    screen.fill((0, 255, 255))
    round_win_text = end_font.render("Player " + str(win + 1) + " wins!!", True, (255, 255, 255))
    round_text = round_font.render("Next Round, BONDS !!", True, (0, 0, 0))
    screen.blit(round_win_text, (275, 295))
    screen.blit(round_text, (275, 365))
    show_score(1)
    pg.display.update()
    mixer.music.unpause()


# game over screen - after round 2
def over_screen(win):
    mixer.music.pause()
    win_sound.play()
    screen.fill((55, 0, 55))
    end_text = end_font.render("Player " + str(win + 1) + " wins!!", True, (255, 255, 255))
    screen.blit(end_text, (300, 320))
    show_score(3)
    pg.display.update()
    mixer.music.unpause()


# print all images at (x,y)
def object_print(image, x, y):
    screen.blit(image, (x, y))


# detecting collision
def collision(px, py, obsx, obsy):
    # calc distance by distance formula
    distance = math.sqrt((obsx - px) ** 2 + (obsy - py) ** 2)
    if distance < 50:
        return True
    else:
        return False


# actions performed after collision
def after_collision(runs, p_turn, num_round, wins):
    pX[p_turn] = 1600
    pY[p_turn] = 1600
    pX_delta[p_turn] = 0
    pY_delta[p_turn] = 0
    num_round += 1
    wait_for_player = True
    while wait_for_player:
        if num_round == 1 or num_round == 3:
            # player change screen
            pchange_screen(num_round - 1)
        elif num_round == 2:
            # round change screen
            round_screen(wins)
            p_reset()
        elif num_round > 3:
            # game over screen and exit
            over_screen(wins)
        for event in pg.event.get():
            if event.type == pg.QUIT:  # closing the window
                runs = False
                wait_for_player = False
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_RETURN:
                    wait_for_player = False
    if num_round > 3:
        runs = False
    p_turn = num_round % 2
    return runs, p_turn, num_round, wins
