# importing libraries and other files
import pygame as pg
from config import *
from pygame import mixer

# setting the fps variable
FPS = 70

# counter for timer
counter = 0
seconds = 0
second_flag = 0  # flag for resetting seconds after round

# speed change flag for obstacles
speed_flag = 0

# speed boost multiplier (speacial feature)
speed = 1

# winner record - {0} palyer 1, {1} player 2
wins = -1

# player turn
p_turn = 0  # {0,1}
num_round = 0  # {0,1,2,3}

# Main loop - that runs the game window
wait = True
runs = True
while runs:
    # time counter : half second is one unit of time
    counter += 1
    if counter == FPS//2:
        counter = 0
        seconds += 1

    # start screen - press enter
    while wait and runs:
        start_screen()
        for event in pg.event.get():
            if event.type == pg.QUIT:  # closing the window
                runs = False
            elif event.type == pg.KEYDOWN and event.key == pg.K_RETURN:
                wait = False

    # main game background
    game_background()
    for event in pg.event.get():
        if event.type == pg.QUIT:  # closing the window
            runs = False
        # p_turn == 0 (player 1) : p_turn == 1 (player 2)
        # player movement
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_SPACE:
                speed = 2   # pressing spacebar increases the player speed by twice
            if event.key == pg.K_LEFT:
                pX_delta[p_turn] = -2.0 * speed
            elif event.key == pg.K_RIGHT:
                pX_delta[p_turn] = 2.0 * speed
            elif event.key == pg.K_UP:
                pY_delta[p_turn] = -2.0 * speed
            elif event.key == pg.K_DOWN:
                pY_delta[p_turn] = 2.0 * speed
        if event.type == pg.KEYUP:
            if event.key == pg.K_SPACE:
                speed = 1
            if event.key == pg.K_LEFT or event.key == pg.K_RIGHT:
                pX_delta[p_turn] = 0
            if event.key == pg.K_UP or event.key == pg.K_DOWN:
                pY_delta[p_turn] = 0
    # condition for moving only when player is in view
    if 0 <= pX[p_turn] < 1216 or 0 <= pY[p_turn] < 656:
        pX[p_turn] += pX_delta[p_turn]
        pY[p_turn] += pY_delta[p_turn]
    else:
        pX_delta[p_turn] = 0
        pY_delta[p_turn] = 0

    # fixed obstacles print
    for i in range(num_fixed):
        object_print(fixed_obsImg[i], fixed_obsX[i], fixed_obsY[i])

    # ground moving obstacles and condition for moving only inside the window
    if ground_obsX[0] >= (1280 - 64):
        ground_obsX_delta[0] = -4
    elif ground_obsX[0] <= 514:
        ground_obsX_delta[0] = +4
    if ground_obsX[1] >= (750 - 64):
        ground_obsX_delta[1] = -4
    elif ground_obsX[1] <= 164:
        ground_obsX_delta[1] = +4
    if ground_obsX[2] >= (1050 - 64):
        ground_obsX_delta[2] = -4
    elif ground_obsX[2] <= 364:
        ground_obsX_delta[2] = +4
    # increasing the speed if any player wins, then k>1 otherwise k = 1
    for i in range(num_ground):
        if p_turn == 0:
            ground_obsX[i] += k1 * ground_obsX_delta[i]
        else:
            ground_obsX[i] += k2 * ground_obsX_delta[i]
        object_print(ground_obsImg[i], ground_obsX[i], ground_obsY[i])

    # water moving obstacles
    for i in range(num_water):
        if water_obsX[i] >= (1280 - 64):
            water_obsX[i] = 0
        # increasing the speed if any player wins, then k>1 otherwise k = 1
        if p_turn == 0:
            water_obsX[i] += k1 * (water_obsX_delta[i])
        else:
            water_obsX[i] += k2 * (water_obsX_delta[i])
        object_print(water_obsImg[i], water_obsX[i], water_obsY[i])

    # condition for keeping the player in view (inside game window)
    if pX[p_turn] <= 0:
        pX[p_turn] = 0
    elif pX[p_turn] >= 1216 and pX[p_turn] != 1600:
        pX[p_turn] = 1216
    elif pX[p_turn] == 1600:
        pX[p_turn] = 1600
    if pY[p_turn] <= 0:
        pY[p_turn] = 0
    elif pY[p_turn] >= 656 and pY[p_turn] != 1600:
        pY[p_turn] = 656
    elif pY[p_turn] == 1600:
        pY[p_turn] = 1600

    # calculating scores
    if num_round <= 1:
        if p_turn == 0:
            for i in range(10):
                if not flag1[i] and (pY[p_turn] <= p1_check[i]):
                    score_round1[0] += p1_score_arr[i]
                    flag1[i] = 1
        else:
            for i in range(10):
                if not flag2[i] and (pY[p_turn] >= p2_check[i]):
                    score_round1[1] += p2_score_arr[i]
                    flag2[i] = 1
    if num_round > 1:
        if p_turn == 0:
            for i in range(10):
                if not flag1[i] and (pY[p_turn] <= p1_check[i]):
                    score_round2[0] += p1_score_arr[i]
                    flag1[i] = 1
        else:
            for i in range(10):
                if not flag2[i] and (pY[p_turn] >= p2_check[i]):
                    score_round2[1] += p2_score_arr[i]
                    flag2[i] = 1

    # deciding winner for a round
    if num_round == 1:
        if score_round1[0] > score_round1[1]:
            wins = 0
        elif score_round1[0] < score_round1[1]:
            wins = 1
        else:
            if round1_time[0] > round1_time[1]:
                wins = 1
            else:
                wins = 0
    if num_round == 3:
        if score_round2[0] > score_round2[1]:
            wins = 0
        elif score_round2[0] < score_round2[1]:
            wins = 1
        else:
            if round2_time[0] > round2_time[1]:
                wins = 1
            else:
                wins = 0

    # calculating the time for each player turn
    if not second_flag and num_round == 2:
        seconds = 0
        second_flag = 1
    if num_round == 0:
        round1_time[0] = seconds
    elif num_round == 1:
        round1_time[1] = seconds - round1_time[0]
    elif num_round == 2:
        round2_time[0] = seconds
    elif num_round == 3:
        round2_time[1] = seconds - round2_time[0]

    # checking collisions
    coll = False
    if not coll:
        for i in range(num_fixed):
            coll = collision(pX[p_turn], pY[p_turn], fixed_obsX[i], fixed_obsY[i])
            if coll:
                collision_sound.play()
                collect = after_collision(runs, p_turn, num_round, wins)
                runs = collect[0]
                p_turn = collect[1]
                num_round = collect[2]
                wins = collect[3]
                break
    if not coll:
        for i in range(num_ground):
            coll = collision(pX[p_turn], pY[p_turn], ground_obsX[i], ground_obsY[i])
            if coll:
                collision_sound.play()
                collect = after_collision(runs, p_turn, num_round, wins)
                runs = collect[0]
                p_turn = collect[1]
                num_round = collect[2]
                wins = collect[3]
                break
    if not coll:
        for i in range(num_water):
            coll = collision(pX[p_turn], pY[p_turn], water_obsX[i], water_obsY[i])
            if coll:
                collision_sound.play()
                collect = after_collision(runs, p_turn, num_round, wins)
                runs = collect[0]
                p_turn = collect[1]
                num_round = collect[2]
                wins = collect[3]
                break

    # reaching the final line
    if p_turn == 0:
        if pY[p_turn] == 0:
            collect = after_collision(runs, p_turn, num_round, wins)
            runs = collect[0]
            p_turn = collect[1]
            num_round = collect[2]
            wins = collect[3]
    else:
        if pY[p_turn] == 656:
            collect = after_collision(runs, p_turn, num_round, wins)
            runs = collect[0]
            p_turn = collect[1]
            num_round = collect[2]
            wins = collect[3]

    # changing difficulty - increasing the speed multiplier for moving obstacles
    if num_round > 1:
        if not speed_flag:
            if wins == 0:
                k1 += 0.5
                k2 = 1
            else:
                k2 += 0.5
                k1 = 1
            speed_flag = 1

    # score print
    show_score(num_round)

    # player print
    object_print(pImg[0], pX[0], pY[0])
    object_print(pImg[1], pX[1], pY[1])

    # setting FPS = 70
    clock.tick(FPS)

    # updating the display in every iteration
    pg.display.update()
